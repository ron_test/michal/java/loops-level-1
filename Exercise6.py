def count_odd_and_even(num_list):
    even_counter = 0
    odd_counter = 0
    for i in num_list:
        if i % 2 == 0:
            even_counter += 1
        else:
            odd_counter += 1

    print("Number of even numbers: " + str(even_counter))
    print("Number eof odd numbers: " + str(odd_counter))


if __name__ == "__main__":
    l = [1, 2, 3, 4, 5]
    count_odd_and_even(l)
