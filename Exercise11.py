age = int(input("Enter the dog's age in human years: "))

dog_years = 0

for i in range(1, age+1):
    if i == 1 or i == 2:
        dog_years += 10.5
    else:
        dog_years += 4

print("The dog's age in dog's years is: " + str(dog_years))
