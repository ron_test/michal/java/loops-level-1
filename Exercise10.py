def print_two_stars(rows):
    for i in range(rows):
        print("*  *")
        print()


if __name__ == "__main__":
    print("***")
    print()
    print_two_stars(2)
    print("*****")
    print()
    print_two_stars(3)
