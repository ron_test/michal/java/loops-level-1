s = input("Enter a string: ")

letters_counter = 0
digit_counter = 0

for i in s:
    if i.isalpha():
        letters_counter += 1
    elif i.isnumeric():
        digit_counter += 1

print("Letters: " + str(letters_counter))
print("Digits: " + str(digit_counter))
