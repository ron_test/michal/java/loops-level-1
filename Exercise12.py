x = int(input("Enter x side length: "))
y = int(input("Enter y side length: "))
z = int(input("Enter z side length: "))

if x == y:
    if y == z:
        print("An equilateral triangle")
    else:
        print("An isosceles triangle")
elif y != z:
    print("A scalene triangle")
