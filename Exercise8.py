m = int(input("Enter the number of rows: "))
n = int(input("Enter the number of columns: "))

# Create a matrix filled with zeros
matrix = [[i*j for j in range(n)] for i in range(m)]

for i in matrix:
    for j in i:
        print(j, end=" ")

    print()