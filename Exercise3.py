max_number = int(input("Enter the max number: "))
divided_by = int(input("Enter the divider: "))

num_list = []

for i in range(1, max_number):
    if i % divided_by == 0:
        num_list.append(str(i))

str_num = ', '.join(num_list)

print("Between 0 to {} => {} are the only numbers divided by {}"
      .format(max_number, str_num, divided_by))
print("Number of divided numbers: " + str(len(num_list)))


